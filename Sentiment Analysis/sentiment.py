class Sentiment:
    def __init__(self, targetScore, targetLabel, documentScore, documentLabel):
        self.targetScore = targetScore
        self.targetLabel = targetLabel
        self.documentScore = documentScore
        self.documentLabel = documentLabel