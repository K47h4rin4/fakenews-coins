import json
from watson_developer_cloud import NaturalLanguageUnderstandingV1
import watson_developer_cloud.natural_language_understanding.features.v1 \
  as Features
from collections import namedtuple
from sentiment import Sentiment

class SentimentAnalyzer(object):

    def calculateSentiment(t):
        
        NATURAL_LANGUAGE_UNDERSTANDING = NaturalLanguageUnderstandingV1(
        username="8f0bc9a7-2359-4604-a1b6-1674d9132f87",
        password="lkGzFzgct7hb",
        version="2017-02-27")

        response = NATURAL_LANGUAGE_UNDERSTANDING.analyze(
            text= t,
            features=[
                Features.Sentiment(
                # Sentiment options
                targets=[
                "Amazon"
                ]
                )
            ]
        )

        targetScore = float(response['sentiment']['targets'][0]['score'])
        targetLabel = response['sentiment']['targets'][0]['label']
        documentScore = float(response['sentiment']['document']['score'])
        documentLabel = response['sentiment']['document']['label']

        return Sentiment(targetScore, targetLabel, documentScore, documentLabel)
    