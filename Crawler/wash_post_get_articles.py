import pandas as pd
import re
from bs4 import BeautifulSoup as bs
from urllib.request import urlopen
from robobrowser import RoboBrowser
import MySQLdb
from news_article import NewsArticle
    
db = MySQLdb.connect(host="localhost", port=3306,
                     user="root", passwd="SeiLuk123", db="coins", charset="utf8", use_unicode=True)
db.charset="utf8"
browser = RoboBrowser(history=False)

def writeArticleToDB(date, newspaper ,url, heading, text):
    cursor = db.cursor()
    try:
        cursor.execute(
            """INSERT INTO news_article (date, newspaper, url, heading, content)
                VALUES(STR_TO_DATE(%s, '%%Y/%%m/%%d'), %s, %s, %s, %s) """,
                (date ,newspaper, url, heading, text)
                )
        db.commit()
    except:     
        db.rollback()

    cursor.close()
    

def scrapeUrl(articleUrl):
    browser.open(articleUrl)
    heading = getHeading()
    if heading == '':
        heading = getHeadingBefore2010()
    text = getText()
    if text == '':
        text = getTextBefore2010()
    date = getDate(articleUrl)
    newsArticle = NewsArticle(articleUrl, date, heading, text)
    return newsArticle

def getText():
    try:
        html = bs(str(browser.parsed), "html.parser")
        article = html.body.findAll('article')
        paragraphs = article[0].findAll('p')
        text = ''
        for i in range(len(paragraphs)):
            text += paragraphs[i].text
        
    except Exception:
        text = ''

    return text


def getTextBefore2010():
    try:
        html = bs(str(browser.parsed), "html.parser")
        article = html.body.findAll('div', {'id' : 'article_body'})
        paragraphs = article[0].findAll('p')
        text = ''
        for i in range(len(paragraphs)):
            text += paragraphs[i].text
        
    except Exception:
        text = ''

    return text


def getHeading():
    try:
        html = bs(str(browser.parsed), "html.parser")
        headline = html.body.findAll('h1', {'itemprop' : 'headline'})[0].text
        if headline == 'Sorry, we can’t seem to find the page you’re looking for.':
            return ''
        else:
            return headline
    except Exception:
        return ''

def getHeadingBefore2010():
    try:
        html = bs(str(browser.parsed), "html.parser")
        headline = html.body.findAll('h1')[0].text
        if headline == 'Sorry, we can’t seem to find the page you’re looking for.':
            return ''
        else:
            return headline
    except Exception:
        return ''
    
    

def getDate(text):
    try:
        m = re.search('(\d{4}\/\d{2}\/\d{1,2}).*', text)
        if m:
            date = m.group(1)
            return date
        return ''
    except Exception:
        return ''
    
    

df = pd.read_excel("wahington_post_articles6.xlsx",
                   skiprows=1, sheetname="Sheet1")
df.columns=['id', 'url']

urls = df['url'].values


for i in range(len(urls)):
    try:
        newsArticle = scrapeUrl(urls[i])
        print(newsArticle.url)
        writeArticleToDB(newsArticle.date, 'Washington Post', newsArticle.url, newsArticle.heading, newsArticle.text)
    except Exception:
        print('Skiped news article: ' + urls[i])
    

